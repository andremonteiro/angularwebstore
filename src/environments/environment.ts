// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA-VRMB2PShyxnD6-Qb9Ff_aA_qHoNSdu4',
    authDomain: 'project-aisa.firebaseapp.com',
    databaseURL: 'https://project-aisa.firebaseio.com',
    projectId: 'project-aisa',
    storageBucket: 'project-aisa.appspot.com',
    messagingSenderId: '683723468956',
  },
  toastr: {
    maxOpened: 3,
    autoDismiss: true,
    preventDuplicates: true,
    preventOpenDuplicates: true,
  },
  timeoutDuration: 10000,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
