export const appRoutes = [
  {
    path: 'admin',
    loadChildren: './modules/admin/admin.module#AdminModule',
  },
  {
    path: '',
    loadChildren: './modules/store/store.module#StoreModule',
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];
