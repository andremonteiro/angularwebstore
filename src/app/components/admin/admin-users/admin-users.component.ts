import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy,
} from '@angular/core';
import { User } from 'src/app/view-model';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {
  LoadingService,
  TranslateService,
  ModalService,
} from 'src/app/services';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.scss'],
})
export class AdminUsersComponent implements OnInit, OnDestroy {
  users: User[];

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();

  constructor(
    private route: ActivatedRoute,
    private chRef: ChangeDetectorRef,
    private translateService: TranslateService,
    private loading: LoadingService,
    private toastr: ToastrService,
    private modal: ModalService
  ) {}

  ngOnInit() {
    const users = this.route.snapshot.data.users;

    if (typeof users === 'string') {
      setTimeout(() => this.toastr.error('GENERAL_ERROR'), 11);
    } else {
      this.users = users;
    }

    this.chRef.detectChanges();

    this.updateDataTable();

    this.loading.hide();
  }

  ngOnDestroy() {
    this.dtTrigger.unsubscribe();
    this.chRef.detach();
  }

  updateDataTable() {
    if (!this.dtElement.dtInstance) {
      this.dtTrigger.next();
    } else {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
  }
}
