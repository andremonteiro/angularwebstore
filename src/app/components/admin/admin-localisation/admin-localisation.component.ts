import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy,
} from '@angular/core';
import {
  TranslateService,
  LoadingService,
  ModalService,
} from 'src/app/services';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ModalData } from 'src/app/view-model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin-localisation',
  templateUrl: './admin-localisation.component.html',
  styleUrls: ['./admin-localisation.component.scss'],
})
export class AdminLocalisationComponent implements OnInit, OnDestroy {
  translations: any;
  translationTable: any = {};
  langKeys: string[] = [];
  selectedLanguage = 'en';

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  dtOptions: DataTables.Settings = {
    pageLength: 25,
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private loading: LoadingService,
    private chRef: ChangeDetectorRef,
    private modal: ModalService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.translations = this.route.snapshot.data.translations;

    this.route.queryParams.subscribe(params => {
      this.selectedLanguage = params['lang'] ? params['lang'] : 'pt';
    });

    // tslint:disable-next-line:forin
    for (const key in this.translations) {
      if (!this.langKeys.find(k => k === key)) {
        this.langKeys.push(key);
      }
    }

    this.getTranslationsTable();

    this.chRef.detectChanges();

    this.updateDataTable();

    this.loading.hide();
  }

  ngOnDestroy() {
    this.chRef.detach();
    this.dtTrigger.unsubscribe();
  }

  getTranslationsTable() {
    this.langKeys.forEach(lang => {
      const pairs = [];

      // tslint:disable-next-line:forin
      for (const key in this.translations[lang]) {
        pairs.push({
          key: key,
          val: this.translations[lang][key],
        });
      }

      this.translationTable[lang] = pairs;
    });

    console.log(this.translationTable);

    // return pairs;
  }

  onSelectLanguageChange($event) {
    const newLang = $event.target.value;
    this.router.navigate(['admin/localisation'], {
      queryParams: { lang: newLang },
    });
    this.updateDataTable();
  }

  updateDataTable() {
    if (!this.dtElement.dtInstance) {
      this.dtTrigger.next();
    } else {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        // dtInstance.clear();
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
  }

  onDeleteButtonClick(key: string) {
    const modalData: ModalData = {
      title: this.translate.translate('MODAL_LOCALISATION_DELETE_TITLE'),
      description: this.translate.translate(
        'MODAL_LOCALISATION_DELETE_DESCRIPTION'
      ),
      buttonPrimary: {
        text: this.translate.translate('DELETE'),
        class: 'danger',
      },
    };

    this.modal.setData(modalData);
    this.modal.setCallback(() => {
      this.loading.show();

      this.translate.deleteTranslation(this.selectedLanguage, key).then(() => {
        this.translate.getLocalisationStrings().then(data => {
          this.translations = data;

          this.updateDataTable();

          this.loading.hide();
          this.toastr.success('TOAST_LOCALISATION_DELETE_SUCCESS');
        });
      });
    });
  }
}
