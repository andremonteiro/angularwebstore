import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLocalisationComponent } from './admin-localisation.component';

describe('AdminLocalisationComponent', () => {
  let component: AdminLocalisationComponent;
  let fixture: ComponentFixture<AdminLocalisationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLocalisationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLocalisationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
