import { Component, OnInit } from '@angular/core';
import { LoadingService, TranslateService } from 'src/app/services';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-admin-localisation-edit',
  templateUrl: './admin-localisation-edit.component.html',
  styleUrls: ['./admin-localisation-edit.component.scss'],
})
export class AdminLocalisationEditComponent implements OnInit {
  translation: any;
  lang: string;

  translationForm: FormGroup;
  editor = ClassicEditor;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private location: Location,
    private loading: LoadingService,
    private translateService: TranslateService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.lang = this.route.snapshot.params['lang'];

    const translations = this.route.snapshot.data.translations;
    const id = this.route.snapshot.params['id'];

    const val = translations[this.lang][id];

    this.translation = {
      key: val || val === '' ? id : '',
      val: val ? val : '',
    };

    this.translationForm = this.createFormGroup();

    // this.checkboxNoHtml = this.htmlToPlainText(val) === val;

    this.loading.hide();
  }

  createFormGroup(): FormGroup {
    return this.formBuilder.group({
      key: this.translation.key,
      val: this.translation.val,
      isPlaintext:
        this.htmlToPlainText(this.translation.val) === this.translation.val,
    });
  }

  onCancelButtonClick() {
    this.location.back();
  }

  onSubmitButtonClick() {
    this.loading.show();

    this.translateService.getLocalisationStrings().then(data => {
      const translations = data[this.lang];

      translations[this.translationForm.value.key] = this.translationForm.value
        .isPlaintext
        ? this.htmlToPlainText(this.translationForm.value.val)
        : this.translationForm.value.val;

      this.translateService
        .updateTranslations(this.lang, translations)
        .then(() => {
          this.loading.hide();
          this.toastr.success(
            this.translateService.translate('TOAST_LOCALISATION_UPDATE_SUCCESS')
          );
          this.location.back();
        });
    });
  }

  htmlToPlainText(str: string) {
    return str.replace(/<[^>]*>/g, '');
  }
}
