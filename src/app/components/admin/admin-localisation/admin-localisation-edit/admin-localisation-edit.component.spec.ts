import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLocalisationEditComponent } from './admin-localisation-edit.component';

describe('AdminLocalisationEditComponent', () => {
  let component: AdminLocalisationEditComponent;
  let fixture: ComponentFixture<AdminLocalisationEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLocalisationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLocalisationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
