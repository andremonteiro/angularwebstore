import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Product, Category } from 'src/app/view-model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductsService, TranslateService } from 'src/app/services';
import { Location } from '@angular/common';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-admin-products-edit',
  templateUrl: './admin-products-edit.component.html',
  styleUrls: ['./admin-products-edit.component.scss'],
})
export class AdminProductsEditComponent implements OnInit {
  product: Product;
  categories: Category[];

  productForm: FormGroup;
  editor = ClassicEditor;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private productsService: ProductsService,
    private loading: LoadingService,
    private toastr: ToastrService,
    private translate: TranslateService,
    private location: Location
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    const categories: Category[] = this.route.snapshot.data.categories;

    if (typeof categories === 'string') {
      this.location.back();
    }

    this.categories = categories;

    const products: Product[] = this.route.snapshot.data.products;

    this.product = products.find(p => p.id === id);

    if (!this.product) {
      this.product = {
        id: '',
        name: '',
        category: null,
        description: '',
        price: 0,
        oldPrice: 0,
        coupon: '',
        imageUrl: '',
        storeName: '',
        url: '',
      };
    }

    this.productForm = this.createFormGroup();

    this.loading.hide();
  }

  createFormGroup(): FormGroup {
    return this.formBuilder.group({
      id: this.product.id,
      name: [this.product.name, [Validators.required]],
      category: this.product.category,
      description: this.product.description,
      price: [this.product.price, [Validators.required]],
      oldPrice: [this.product.oldPrice, [Validators.required]],
      coupon: [this.product.coupon, [Validators.required]],
      imageUrl: [this.product.imageUrl, [Validators.required]],
      storeName: [this.product.storeName, [Validators.required]],
      url: [this.product.url, [Validators.required]],
    });
  }

  onSubmitButtonClick() {
    this.productsService.updateProduct(this.productForm.value).then(() => {
      this.location.back();
      this.toastr.success(
        this.product.id === ''
          ? this.translate.translate('TOAST_PRODUCT_CREATE_SUCCESS')
          : this.translate.translate('TOAST_PRODUCT_UPDATE_SUCCESS')
      );
    });
  }

  onCancelButtonClick() {
    this.location.back();
  }
}
