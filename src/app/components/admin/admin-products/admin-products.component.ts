import {
  Component,
  OnInit,
  ChangeDetectorRef,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import {
  LoadingService,
  ModalService,
  TranslateService,
  ProductsService,
  CategoriesService,
  SettingsService,
} from 'src/app/services';
import { Product, Category, ModalData } from 'src/app/view-model';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DataTableDirective } from 'angular-datatables';
import { Subject, from } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.scss'],
})
export class AdminProductsComponent implements OnInit, OnDestroy {
  products: Product[];

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();

  showIds = false;

  constructor(
    private route: ActivatedRoute,
    private chRef: ChangeDetectorRef,
    private productsService: ProductsService,
    private categoriesService: CategoriesService,
    private settingsService: SettingsService,
    private loading: LoadingService,
    private modal: ModalService,
    private toastr: ToastrService,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.showIds = this.settingsService.settings.admin.showIds;

    const products = this.route.snapshot.data.products;

    if (typeof products === 'string') {
      setTimeout(() => this.toastr.error('GENERAL_ERROR'), 11);
    } else {
      this.products = products;
    }

    this.categoriesService
      .getCategories()
      .subscribe((categories: Category[]) => {
        products.forEach(p => {
          if (typeof categories === 'string') {
            return;
          }
          const cat = categories.find(c => c.id === p.category);
          if (cat) {
            p.category = cat.name;
          } else {
            p.category = '';
          }
        });

        this.chRef.detectChanges();

        this.updateDataTable();

        this.loading.hide();
      });
  }

  ngOnDestroy() {
    this.dtTrigger.unsubscribe();
    this.chRef.detach();
  }

  updateDataTable() {
    if (!this.dtElement.dtInstance) {
      this.dtTrigger.next();
    } else {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
  }

  onDeleteButtonClick(productId: string) {
    console.log(productId);
    const modalData: ModalData = {
      title: this.translateService.translate('MODAL_PRODUCT_DELETE_TITLE'),
      description: this.translateService.translate(
        'MODAL_PRODUCT_DELETE_DESCRIPTION'
      ),
      buttonPrimary: {
        text: this.translateService.translate('DELETE'),
        class: 'danger',
      },
    };

    this.modal.setData(modalData);
    this.modal.setCallback(() => {
      this.loading.show();

      from(this.productsService.deleteProduct(productId)).subscribe(() => {
        this.productsService
          .getProducts()
          .pipe(take(1))
          .subscribe((data: Product[]) => {
            this.loading.hide();

            this.toastr.success(
              this.translateService.translate('TOAST_PRODUCT_DELETE_SUCCESS')
            );

            this.chRef.detectChanges();

            this.products = data;

            this.updateDataTable();
          });
      });
    });
  }
}
