import {
  Component,
  OnInit,
  ChangeDetectorRef,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { Category, ModalData } from 'src/app/view-model';
import {
  TranslateService,
  LoadingService,
  ModalService,
  CategoriesService,
  SettingsService,
} from 'src/app/services';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { from, Subject } from 'rxjs';

import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-admin-categories',
  templateUrl: './admin-categories.component.html',
  styleUrls: ['./admin-categories.component.scss'],
})
export class AdminCategoriesComponent implements OnInit, OnDestroy {
  categories: Category[];

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();

  showIds = false;

  constructor(
    private route: ActivatedRoute,
    private chRef: ChangeDetectorRef,
    private categoriesService: CategoriesService,
    private translateService: TranslateService,
    private settingsService: SettingsService,
    private toastr: ToastrService,
    private loading: LoadingService,
    private modal: ModalService
  ) {}

  ngOnInit() {
    this.showIds = this.settingsService.settings.admin.showIds;

    const categories = this.route.snapshot.data.categories;

    if (typeof categories === 'string') {
      setTimeout(
        () =>
          this.toastr.error(
            this.translateService.translate('TOAST_ERROR_GENERAL')
          ),
        11
      );
    } else {
      this.categories = categories;
    }

    this.chRef.detectChanges();

    this.updateDataTable();

    this.loading.hide();
  }

  ngOnDestroy() {
    this.chRef.detach();
    this.dtTrigger.unsubscribe();
  }

  getParentCategoryName(categoryId: string) {
    const name = this.categories.find(c => c.id === categoryId).name;
    return name ? name : '';
  }

  onDeleteButtonClick(categoryId) {
    const modalData: ModalData = {
      title: this.translateService.translate('MODAL_CATEGORY_DELETE_TITLE'),
      description: this.translateService.translate(
        'MODAL_CATEGORY_DELETE_DESCRIPTION'
      ),
      buttonPrimary: {
        text: this.translateService.translate('DELETE'),
        class: 'danger',
      },
    };

    this.modal.setData(modalData);
    this.modal.setCallback(() => {
      this.loading.show();

      from(this.categoriesService.deleteCategory(categoryId)).subscribe(
        (data: any) => {
          this.loading.hide();

          if (data.warning) {
            this.toastr.warning(data.data);
            return;
          }

          this.toastr.success(
            this.translateService.translate('TOAST_CATEGORY_DELETE_SUCCESS')
          );

          this.chRef.detectChanges();

          this.categories = data;

          this.updateDataTable();
        }
      );
    });
  }

  updateDataTable() {
    if (!this.dtElement.dtInstance) {
      this.dtTrigger.next();
    } else {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }
  }
}
