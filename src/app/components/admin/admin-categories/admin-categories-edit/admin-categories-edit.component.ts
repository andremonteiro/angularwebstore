import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/view-model';
import { CategoriesService, TranslateService } from 'src/app/services';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-admin-categories-edit',
  templateUrl: './admin-categories-edit.component.html',
  styleUrls: ['./admin-categories-edit.component.scss'],
})
export class AdminCategoriesEditComponent implements OnInit {
  category: Category = {
    id: '',
    name: '',
    parent: null,
  };
  categoryForm: FormGroup;

  categories: Category[];

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private categoriesService: CategoriesService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private translate: TranslateService,
    private loading: LoadingService
  ) {}

  ngOnInit() {
    this.loading.hide();

    const categories = this.route.snapshot.data.categories;
    const id = this.route.snapshot.paramMap.get('id');

    if (typeof categories === 'string') {
      setTimeout(() => this.toastr.error('GENERAL_ERROR'), 11);
    } else {
      this.categories = categories;
    }

    this.category = this.categories.find(c => c.id === id);

    if (!this.category) {
      this.category = {
        id: '',
        name: '',
        parent: null,
      };
    }

    this.categoryForm = this.createFormGroup();
  }

  createFormGroup(): FormGroup {
    return this.formBuilder.group({
      id: this.category.id,
      name: this.category.name,
      parent: this.category.parent,
    });
  }

  onSubmitButtonClick() {
    this.categoriesService.updateCategory(this.categoryForm.value).then(() => {
      this.location.back();
      this.toastr.success(
        this.category.id
          ? this.translate.translate('TOAST_CATEGORY_CREATE_SUCCESS')
          : this.translate.translate('TOAST_CATEGORY_UPDATE_SUCCESS')
      );
    });
  }

  onCancelButtonClick() {
    this.location.back();
  }

  compare(val1, val2) {
    return val1 === val2;
  }
}
