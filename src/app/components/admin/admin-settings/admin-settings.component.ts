import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/services/settings/settings.service';
import { from } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { TranslateService, LoadingService } from 'src/app/services';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.scss'],
})
export class AdminSettingsComponent implements OnInit {
  settings: any;
  settingsForm = new FormGroup({});
  fields: FormlyFieldConfig[] = [];

  constructor(
    private route: ActivatedRoute,
    private settingsService: SettingsService,
    private translateService: TranslateService,
    private loading: LoadingService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.settings = this.route.snapshot.data.settings;

    this.buildSettingsForm(this.settings);

    this.loading.hide();
  }

  buildSettingsForm(json: any) {
    // tslint:disable-next-line:forin
    for (const key in json) {
      this.fields.push({
        template: this.translateService.translate(
          'SETTINGS_KEY_' + key.toUpperCase()
        ),
        className: 'h5',
      });

      const children = {
        key: key,
        fieldGroupClassName: 'row',
        fieldGroup: [],
      };

      // tslint:disable-next-line:forin
      for (const ckey in json[key]) {
        let type = 'checkbox';
        let multiple = false;
        let options = null;

        if (typeof json[key][ckey] === 'string') {
          type = 'input';
        }

        if (typeof json[key][ckey] === 'number') {
          type = 'input';
        }

        if (typeof json[key][ckey] === 'object') {
          if (ckey === 'displayLanguages') {
            type = 'select';
            multiple = true;
            options = [];
            // tslint:disable-next-line:forin
            for (const lang in this.translateService.translations) {
              options.push({
                label: this.translateService.translate(
                  'LANGUAGE_' + lang.toUpperCase()
                ),
                value: lang,
              });
            }
          }
        }

        console.log(typeof json[key][ckey], json[key][ckey]);

        children.fieldGroup.push({
          key: ckey,
          type: type,
          className: 'col-md-6',
          templateOptions: {
            label: this.translateService.translate(
              'SETTINGS_KEY_' + ckey.toUpperCase()
            ),
            multiple: multiple,
            options: options,
          },
        });
      }

      this.fields.push(children);
    }
  }

  onSubmitButtonClick() {
    this.loading.show();

    this.settingsService
      .saveSettings(this.settingsForm.value)
      .then(() => {
        this.settingsForm.markAsPristine();

        this.toastr.success(
          this.translateService.translate('TOAST_SETTINGS_SAVE_SUCCESS')
        );

        this.loading.hide();
      })
      .catch(err => {
        this.toastr.error(
          this.translateService.translate('TOAST_SETTINGS_SAVE_ERROR') + err
        );

        this.loading.hide();
      });
  }
}
