import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { TranslateService } from 'src/app/services';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  constructor(private title: Title, private translate: TranslateService) {}

  ngOnInit() {
    this.title.setTitle(
      this.translate.translate('APP_TITLE_ADMIN') +
        ' ' +
        this.translate.translate('APP_DIVISION') +
        ' ' +
        this.translate.translate('APP_NAME')
    );
  }
}
