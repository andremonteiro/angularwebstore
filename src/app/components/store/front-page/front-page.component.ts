import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import {
  TranslateService,
  LoadingService,
  SettingsService,
} from 'src/app/services';
import { FormControl } from '@angular/forms';
import { Product, Category } from 'src/app/view-model';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'store-front-page',
  templateUrl: './front-page.component.html',
  styleUrls: ['./front-page.component.scss'],
})
export class FrontPageComponent implements OnInit {
  products: Product[];
  categories: Category[];

  searchInput = new FormControl();
  categoryId: string;
  categoryFilter: string[];

  numItemsToDisplay = 12;

  infoCardCollapsed = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private settings: SettingsService,
    private loading: LoadingService,
    private translate: TranslateService,
    private title: Title
  ) {}

  ngOnInit() {
    this.products = this.route.snapshot.data.products;

    this.categories = this.route.snapshot.data.categories;

    this.route.queryParams.subscribe(params => {
      this.categoryId = params['category'] ? params['category'] : undefined;
      this.onCategoryChange(this.categoryId);
      this.searchInput.setValue(params['search'] ? params['search'] : '');
    });

    const numItems = this.settings.getSetting('store', 'numItemsToDisplay');
    this.numItemsToDisplay =
      numItems && numItems > 0 ? numItems : this.numItemsToDisplay;

    this.title.setTitle(
      this.translate.translate('APP_TITLE_FRONT_PAGE') +
        ' ' +
        this.translate.translate('APP_DIVISION') +
        ' ' +
        this.translate.translate('APP_NAME')
    );

    this.loading.hide();
  }

  onCategoryChange($event) {
    this.categoryId = $event;

    if ($event) {
      this.categoryFilter = [$event];

      this.categories.forEach(c => {
        if (c.parent && c.parent === $event) {
          this.categoryFilter.push(c.id);
        }
      });
    } else {
      this.categoryFilter = null;
    }

    this.updateUrl();
  }

  onSearchInputChange($event) {
    this.updateUrl();
  }

  updateUrl() {
    this.router.navigate(['.'], {
      queryParams: {
        category: this.categoryId,
        search:
          this.searchInput.value !== null &&
          this.searchInput.value.trim() !== ''
            ? this.searchInput.value
            : null,
      },
    });
  }

  onSearchClearClick($event) {
    this.searchInput.reset();
    this.updateUrl();
  }

  showFrontPageCard() {
    return this.settings.getSetting('store', 'showFrontPageCard')
      ? true
      : false;
  }
}
