import { Component, OnInit } from '@angular/core';
import { Product, User } from 'src/app/view-model';
import { ActivatedRoute, Router } from '@angular/router';
import {
  LoadingService,
  AuthService,
  TranslateService,
} from 'src/app/services';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  user: User;
  wishlist: Product[];

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute,
    private loading: LoadingService,
    private router: Router,
    private title: Title,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    const products: Product[] = this.route.snapshot.data.products;
    this.user = this.route.snapshot.data.user;

    if (!this.user) {
      this.router.navigate(['']);
      return;
    }

    this.auth.user.subscribe(user => {
      if (!user) {
        this.router.navigate(['']);
      }
    });

    if (this.user.wishlist && this.user.wishlist.length > 0) {
      this.wishlist = products.filter(p => {
        return this.user.wishlist.indexOf(p.id) >= 0;
      });
    }

    this.title.setTitle(
      this.translate.translate('APP_TITLE_WISHLIST') +
        ' ' +
        this.translate.translate('APP_DIVISION') +
        ' ' +
        this.translate.translate('APP_NAME')
    );

    this.loading.hide();
  }
}
