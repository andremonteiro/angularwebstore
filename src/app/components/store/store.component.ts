import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'src/app/services';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss'],
})
export class StoreComponent implements OnInit {
  constructor(private title: Title, private translate: TranslateService) {}

  ngOnInit() {
    const title = this.translate.translate('APP_NAME');
    this.title.setTitle(title);
  }
}
