import { Component, OnInit } from '@angular/core';
import {
  LoadingService,
  TranslateService,
  UsersService,
  AuthService,
} from 'src/app/services';
import { Product, Category } from 'src/app/view-model';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss'],
})
export class DetailPageComponent implements OnInit {
  product: Product;
  categories: Category[] = [];

  discount: number;
  discountPercentage: number;

  isUserLoggedIn: boolean;
  isProductInWishlist: boolean;

  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private loading: LoadingService,
    private translate: TranslateService,
    private title: Title,
    private toastr: ToastrService,
    private auth: AuthService,
    private usersService: UsersService
  ) {}

  ngOnInit() {
    const productId = this.route.snapshot.params['id'];
    const products: Product[] = this.route.snapshot.data.products;
    this.product = products.find(p => p.id === productId);

    if (!this.product) {
      this.router.navigate(['']);
      return;
    }

    const categories: Category[] = this.route.snapshot.data.categories;
    const prodCategory = categories.find(c => c.id === this.product.category);

    if (prodCategory.parent) {
      const parentCategory = categories.find(c => c.id === prodCategory.parent);
      this.categories.push(parentCategory);
    }

    this.categories.push(prodCategory);

    // Calculate discount
    this.discount = (1 - +this.product.price / +this.product.oldPrice) * 100;
    this.discountPercentage = Math.floor(
      (1 - this.product.price / this.product.oldPrice) * 100
    );

    this.title.setTitle(
      this.product.name +
        ' ' +
        this.translate.translate('APP_DIVISION') +
        ' ' +
        this.translate.translate('APP_NAME')
    );

    // Check if product is in the user's wishlist
    this.auth.user.subscribe(user => {
      this.loading.hide();

      if (!user) {
        this.isUserLoggedIn = false;
        return;
      }

      this.isUserLoggedIn = true;

      if (user.wishlist && user.wishlist.indexOf(productId) >= 0) {
        this.isProductInWishlist = true;
      } else {
        this.isProductInWishlist = false;
      }
    });
  }

  onBreadcrumbCategoryClick(category: Category) {
    this.router.navigate([''], {
      queryParams: {
        category: category.id,
      },
    });
  }

  onBackButtonClick() {
    this.location.back();
  }

  onCouponInputClick($event) {
    const input = $event.target;
    input.select();
    document.execCommand('copy');
    this.toastr.success(this.translate.translate('TOAST_COUPON_COPY'));
  }

  updateWishlist() {
    this.loading.show();

    this.usersService
      .updateUserWishlist(this.product.id, this.isProductInWishlist)
      .then(() => {
        this.loading.hide();
      });
  }
}
