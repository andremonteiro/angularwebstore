import { Component, OnInit } from '@angular/core';
import {
  LoadingService,
  AuthService,
  TranslateService,
} from 'src/app/services';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  private redirectUrl = '/';

  constructor(
    private auth: AuthService,
    private loading: LoadingService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translate: TranslateService,
    private title: Title
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params.redirect) {
        this.redirectUrl = params.redirect;
      }
    });

    this.title.setTitle(
      this.translate.translate('APP_TITLE_LOGIN') +
        ' ' +
        this.translate.translate('APP_DIVISION') +
        ' ' +
        this.translate.translate('APP_NAME')
    );

    this.loading.hide();
  }

  loginWithGoogle() {
    this.auth
      .loginWithGoogle()
      .then(() => {
        this.router.navigate([this.redirectUrl]);
        this.toastr.success(this.translate.translate('TOAST_LOGIN_SUCCESS'));
      })
      .catch(err => {
        this.toastr.error(this.translate.translate('TOAST_LOGIN_ERROR'));
      });
  }

  loginWithFacebook() {
    // TODO
  }
}
