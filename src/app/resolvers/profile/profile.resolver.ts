import { Injectable } from '@angular/core';
import { User } from 'src/app/view-model';
import { AuthService, LoadingService } from 'src/app/services';
import { Resolve } from '@angular/router';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ProfileResolver implements Resolve<User | string> {
  constructor(private auth: AuthService, private loading: LoadingService) {}

  resolve() {
    this.loading.show();
    return this.auth.user.pipe(take(1));
  }
}
