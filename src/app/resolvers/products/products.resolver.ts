import { Injectable } from '@angular/core';
import { Product } from 'src/app/view-model';
import { Resolve } from '@angular/router';
import { ProductsService } from 'src/app/services';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ProductsResolver implements Resolve<Product[] | string> {
  constructor(
    private productsService: ProductsService,
    private loading: LoadingService
  ) {}

  resolve() {
    this.loading.show();
    return this.productsService.getProducts().pipe(take(1));
  }
}
