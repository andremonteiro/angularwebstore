import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { User } from 'src/app/view-model';
import { UsersService, LoadingService } from 'src/app/services';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UsersResolver implements Resolve<User[] | string> {
  constructor(
    private usersService: UsersService,
    private loading: LoadingService
  ) {}

  resolve() {
    this.loading.show();
    return this.usersService.getUsers().pipe(take(1));
  }
}
