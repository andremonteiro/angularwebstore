import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { CategoriesService } from 'src/app/services';
import { Category } from 'src/app/view-model';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CategoriesResolver implements Resolve<Category[] | string> {
  constructor(
    private categoriesService: CategoriesService,
    private loading: LoadingService
  ) {}

  resolve() {
    this.loading.show();
    return this.categoriesService.getCategories().pipe(take(1));
  }
}
