import { TestBed } from '@angular/core/testing';

import { TranslationsResolver } from './translations.resolver';

describe('TranslationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TranslationsResolver = TestBed.get(TranslationsResolver);
    expect(service).toBeTruthy();
  });
});
