import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { TranslateService, LoadingService } from 'src/app/services';
import { from } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TranslationsResolver implements Resolve<any> {
  constructor(
    private translate: TranslateService,
    private loading: LoadingService
  ) {}

  resolve() {
    this.loading.show();
    return from(this.translate.getLocalisationStrings()).pipe(take(1));
  }
}
