import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';
import { SettingsService } from 'src/app/services/settings/settings.service';
import { LoadingService } from 'src/app/services';

@Injectable({
  providedIn: 'root',
})
export class SettingsResolver implements Resolve<any> {
  constructor(
    private settingsService: SettingsService,
    private loading: LoadingService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.loading.show();
    return this.settingsService.getSettings();
  }
}
