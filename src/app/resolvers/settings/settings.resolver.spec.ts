import { TestBed } from '@angular/core/testing';

import { SettingsResolver } from './settings.resolver';

describe('SettingsResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SettingsResolver = TestBed.get(SettingsResolver);
    expect(service).toBeTruthy();
  });
});
