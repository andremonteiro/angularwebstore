import { Component, OnInit, Input } from '@angular/core';
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';
import { SettingsService } from 'src/app/services';

@Component({
  selector: 'app-splash',
  templateUrl: './splash-screen.component.html',
  styleUrls: ['./splash-screen.component.scss'],
  animations: [
    trigger('hide', [
      state(
        'shown',
        style({
          opacity: 1,
        })
      ),
      state(
        'hidden',
        style({
          opacity: 0,
          'z-index': -1,
        })
      ),
      transition('shown => hidden', [animate('500ms')]),
    ]),
  ],
})
export class SplashScreenComponent implements OnInit {
  @Input() show = true;

  constructor() {}

  ngOnInit() {}
}
