import { Component, OnInit, Input } from '@angular/core';
import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
})
export class LoadingComponent implements OnInit {
  @Input() show = false;

  constructor(private loadingService: LoadingService) {}

  ngOnInit() {
    this.loadingService.register(this);
  }
}
