import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ModalData } from 'src/app/view-model';
import { ModalService } from 'src/app/services/modal/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  @Input() data: ModalData;
  @Output() callback: EventEmitter<any> = new EventEmitter();

  constructor(private modalService: ModalService) {}

  ngOnInit() {
    this.data = {
      title: '',
      description: '',
      buttonPrimary: {
        text: '',
      },
    };
    this.modalService.register(this);
  }

  onPrimaryButtonClick() {
    this.callback.emit();
  }
}
