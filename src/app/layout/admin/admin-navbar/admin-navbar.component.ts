import { Component, OnInit, OnChanges } from '@angular/core';
import {
  AuthService,
  TranslateService,
  SettingsService,
} from 'src/app/services';
import { User } from 'src/app/view-model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.scss'],
})
export class AdminNavbarComponent implements OnInit {
  languages: string[] = [];

  user: User;

  showLanguageMenu = true;

  constructor(
    private router: Router,
    private auth: AuthService,
    private settings: SettingsService,
    private toastr: ToastrService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.auth.user.subscribe(user => {
      this.user = user;
      console.log(user);
    });

    // tslint:disable-next-line:forin
    for (const lang in this.translate.translations) {
      this.languages.push(lang);
    }
  }

  onLanguageClick(idx) {
    const lang = this.languages[idx];
    this.translate.use(lang);
  }

  logout() {
    this.auth.logout();
    this.user = null;
    this.toastr.success(this.translate.translate('TOAST_LOGOUT_SUCCESS'));
    this.router.navigate(['']);
  }
}
