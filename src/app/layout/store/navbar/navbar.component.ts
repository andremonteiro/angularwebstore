import { Component, OnInit } from '@angular/core';
import {
  TranslateService,
  AuthService,
  SettingsService,
} from 'src/app/services';
import { User } from 'src/app/view-model';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'store-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  languages: string[] = [];

  user: User = null;

  showLoginLink = false;
  showLanguageMenu = false;

  constructor(
    private translate: TranslateService,
    private auth: AuthService,
    private settings: SettingsService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.auth.user.subscribe((user: User) => {
      this.user = user;
    });

    // tslint:disable-next-line:forin
    // for (const lang in this.translate.translations) {
    //   this.languages.push(lang);
    // }

    this.showLoginLink = this.settings.getSetting('auth', 'isLoginAvailable')
      ? true
      : false;

    this.showLanguageMenu = this.settings.getSetting(
      'localisation',
      'multiLanguage'
    )
      ? true
      : false;

    this.languages = this.settings.getSetting(
      'localisation',
      'displayLanguages'
    );

    if (this.languages.length < 1) {
      this.showLanguageMenu = false;
    }
  }

  onLanguageClick(idx) {
    const lang = this.languages[idx];
    this.translate.use(lang);
  }

  login() {
    this.router.navigate(['/login'], {
      queryParams: {
        redirect: this.router.url !== '/' ? this.router.url : undefined,
      },
    });
  }

  logout() {
    this.auth.logout();
    this.user = null;
    this.toastr.success(this.translate.translate('TOAST_LOGOUT_SUCCESS'));
  }
}
