import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Product } from 'src/app/view-model/product';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from 'src/app/services';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
import { Router } from '@angular/router';

@Component({
  selector: 'store-product-grid',
  templateUrl: './product-grid.component.html',
  styleUrls: ['./product-grid.component.scss'],
})
export class ProductGridComponent implements OnInit, OnChanges {
  @Input() products: Product[];
  @Input() search: string;
  @Input() filter: string[];
  @Input() numItemsToDisplay = 12;

  displayedProductList: Product[] = [];

  couponIdClicked: string;

  currentPage = 1;
  maxNumOfPages = 1;

  constructor(private router: Router) {}

  ngOnInit() {}

  ngOnChanges() {
    this.productListToDisplay();
  }

  productListToDisplay() {
    let list = this.products;

    // Filter
    if (this.filter && this.filter.length > 0) {
      list = list.filter(p => this.filter.find(f => f === p.category));
    }

    // Search
    if (this.search && this.search.trim().length >= 0) {
      list = list.filter(
        p =>
          p.name.toLocaleLowerCase().indexOf(this.search.toLocaleLowerCase()) >=
          0
      );
    }

    this.displayedProductList = list;

    this.updatePagination(1);
  }

  onCardClick($event: any, productId: string) {
    this.router.navigate(['product', productId]);
  }

  // onCouponInputClick($event) {
  //   const input = $event.target;
  //   input.select();
  //   document.execCommand('copy');
  //   this.toastr.success(this.translate.translate('TOAST_COUPON_COPY'));
  // }

  // onCardClick($event, prodId) {
  //   this.onCouponCloseClick(null);

  //   this.couponIdClicked = prodId;

  //   const front = document.querySelector('#front-' + this.couponIdClicked);
  //   front.classList.remove('d-block');
  //   front.classList.add('d-none');

  //   const back = document.querySelector('#coupon-' + this.couponIdClicked);
  //   back.classList.remove('d-none');
  //   back.classList.add('d-block');
  // }

  // onCouponCloseClick($event) {
  //   if (this.couponIdClicked && this.couponIdClicked.trim().length > 0) {
  //     const front = document.querySelector('#front-' + this.couponIdClicked);
  //     front.classList.add('d-block');
  //     front.classList.remove('d-none');

  //     const back = document.querySelector('#coupon-' + this.couponIdClicked);
  //     back.classList.add('d-none');
  //     back.classList.remove('d-block');
  //     this.couponIdClicked = null;
  //   }
  // }

  isShownOnPage(index: number) {
    const startPos = (this.currentPage - 1) * this.numItemsToDisplay;
    const endPos =
      (this.currentPage - 1) * this.numItemsToDisplay + this.numItemsToDisplay;

    return index >= startPos && index < endPos ? true : false;
  }

  updatePagination(numPage: number) {
    if (numPage < 1 || numPage > this.maxNumOfPages) {
      return;
    }

    this.currentPage = numPage;

    if (this.displayedProductList.length < this.numItemsToDisplay) {
      return;
    }

    this.maxNumOfPages = Math.ceil(
      this.displayedProductList.length / this.numItemsToDisplay
    );
  }
}
