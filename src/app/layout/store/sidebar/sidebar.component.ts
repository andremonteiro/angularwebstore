import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  OnDestroy,
} from '@angular/core';
import { Category } from 'src/app/view-model';

@Component({
  selector: 'store-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit, OnDestroy {
  @Input() categories: Category[];
  @Input() activeCategory: string;
  @Output() change: EventEmitter<string> = new EventEmitter();

  sidebarCategories: any[] = [];

  constructor(private chRef: ChangeDetectorRef) {}

  ngOnInit() {
    this.initSidebarLinks();

    this.chRef.detectChanges();

    this.expandCollapsible();
  }

  ngOnDestroy() {
    this.chRef.detach();
  }

  onCategoryClick($event: any, itemId: string) {
    this.activeCategory = itemId;
    this.change.emit(this.activeCategory);
  }

  initSidebarLinks() {
    for (const item of this.categories.filter(c => !c.parent)) {
      this.sidebarCategories.push(item);
    }
    for (const item of this.categories.filter(c => c.parent)) {
      const parent = this.sidebarCategories.find(c => c.id === item.parent);
      if (!parent.children) {
        parent.children = [];
      }
      parent.children.push(item);
    }
  }

  expandCollapsible() {
    if (!this.activeCategory) {
      return;
    }

    const active = this.categories.find(c => c.id === this.activeCategory);

    console.log(active);

    const parentId = active.parent ? active.parent : active.id;

    // const idx = this.sidebarCategories.findIndex(c =>
    //   c.children.find(child => child.id === active.id) ? true : false
    // );

    $('#a-' + parentId).attr('aria-expanded', 'true');
    $('#div-' + parentId).addClass('show');
  }
}
