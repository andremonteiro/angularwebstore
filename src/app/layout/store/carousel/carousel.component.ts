import { Component, OnInit } from '@angular/core';
import { CarouselItem } from 'src/app/view-model';

@Component({
  selector: 'store-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {
  showCarousel: boolean;
  carouselItems: CarouselItem[] = [
    {
      title: '1',
      subtitle: '111',
      imageUrl:
        'https://udemy-images.udemy.com/course/750x422/1352222_6bfa_2.jpg',
      url: '',
    },
    {
      title: '2',
      subtitle: '222',
      imageUrl:
        'https://cdn.lynda.com/course/713378/713378-636576595892468047-16x9.jpg',
      url: '',
    },
    {
      title: '3',
      subtitle: '333',
      imageUrl:
        'https://images.yourstory.com/2016/08/125-fall-in-love.png?auto=compress',
      url: '',
    },
    {
      title: '4',
      subtitle: '444',
      imageUrl:
        'https://www.axonize.com/wp-content/uploads/2017/08/product.jpg',
      url: '',
    },
  ];

  constructor() {}

  ngOnInit() {}

  onCarouselItemClick(item: CarouselItem) {
    // TODO
  }
}
