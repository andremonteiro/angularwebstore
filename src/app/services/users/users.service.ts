import { Injectable } from '@angular/core';
import { User } from 'src/app/view-model';
import {
  AngularFirestoreCollection,
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { TranslateService } from '../translate/translate.service';
import { Observable, of } from 'rxjs';
import { timeout, catchError, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  private userCollection: AngularFirestoreCollection<User>;

  private duration = environment.timeoutDuration;

  constructor(
    private fireAuth: AngularFireAuth,
    private fireStore: AngularFirestore,
    private translate: TranslateService,
    private auth: AuthService
  ) {
    this.userCollection = this.fireStore.collection<User>('users');
  }

  getUsers(): Observable<any> {
    return this.userCollection.valueChanges().pipe(
      timeout(this.duration),
      catchError(error => of(`Request timed out after ${this.duration} ms`))
    );
  }

  async updateUserWishlist(productId: string, remove: boolean) {
    const user = await this.auth.user.pipe(take(1)).toPromise();

    const userRef: AngularFirestoreDocument<any> = this.fireStore.doc(
      `users/${user.uid}`
    );

    if (!user.wishlist) {
      user.wishlist = [];
    }

    const index = user.wishlist.indexOf(productId);

    if (remove) {
      if (index >= 0) {
        user.wishlist.splice(index, 1);
      }
    } else {
      if (index < 0) {
        user.wishlist.push(productId);
      }
    }

    const data: any = {
      wishlist: user.wishlist,
    };

    await userRef.set(data, { merge: true });
  }
}
