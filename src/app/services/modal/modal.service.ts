import { Injectable } from '@angular/core';
import { ModalComponent } from 'src/app/layout/shared/modal/modal.component';
import { ModalData } from 'src/app/view-model';

import * as $ from 'jquery';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  private component: ModalComponent;

  constructor() {}

  register(component: ModalComponent) {
    this.component = component;
  }

  setData(data: ModalData) {
    this.component.data = data;
  }

  setCallback(callback: Function) {
    this.component.callback.subscribe(callback);
  }
}
