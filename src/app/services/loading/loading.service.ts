import { Injectable } from '@angular/core';
import { LoadingComponent } from 'src/app/layout/shared/loading/loading.component';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  loadingComponent: LoadingComponent;

  constructor() {}

  register(component: LoadingComponent) {
    this.loadingComponent = component;
  }

  show() {
    this.loadingComponent.show = true;
  }

  hide() {
    this.loadingComponent.show = false;
  }
}
