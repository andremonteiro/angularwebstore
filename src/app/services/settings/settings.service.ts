import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  private settingsCollection: AngularFirestoreCollection<any>;

  settings: any;

  constructor(private fireStore: AngularFirestore) {
    this.settingsCollection = this.fireStore.collection<any>('settings');
  }

  async getSettings() {
    const coll = await this.settingsCollection
      .get()
      .pipe(take(1))
      .toPromise();

    const settingsType = coll.docs.map(doc => doc.id);

    const docsContent = await this.settingsCollection
      .valueChanges()
      .pipe(take(1))
      .toPromise();

    this.settings = {};

    settingsType.forEach((lang, i) => {
      this.settings[lang] = docsContent[i];
    });

    return this.settings;
  }

  async saveSettings(settings: any) {
    // tslint:disable-next-line:forin
    for (const key in settings) {
      await this.settingsCollection.doc(key).set(settings[key]);
    }

    this.settings = settings;
  }

  getSetting(doc: string, key: string) {
    return this.settings[doc][key];
  }
}
