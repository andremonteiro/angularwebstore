import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'src/app/view-model';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { of, Observable } from 'rxjs';
import { auth } from 'firebase/app';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user: Observable<User>;

  constructor(
    private fireAuth: AngularFireAuth,
    private fireStore: AngularFirestore
  ) {
    this.user = this.fireAuth.authState.pipe(
      switchMap((user: User) => {
        if (user) {
          return this.fireStore.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }

  loginWithGoogle() {
    const provider = new auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  oAuthLogin(provider: any) {
    return this.fireAuth.auth
      .signInWithPopup(provider)
      .then(credential => {
        this.updateUserData(credential.user).catch(err => console.log(err));
      })
      .catch(err => {});
  }

  private updateUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.fireStore.doc(
      `users/${user.uid}`
    );

    const data: any = {
      displayName: user.displayName,
      email: user.email,
      photoURL: user.photoURL,
      loginDate: new Date(),
      uid: user.uid,
    };

    return userRef.set(data, { merge: true });
  }

  logout() {
    this.fireAuth.auth.signOut();
  }
}
