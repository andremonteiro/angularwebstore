import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {
  AngularFirestoreCollection,
  AngularFirestore,
} from '@angular/fire/firestore';
import { Product } from 'src/app/view-model';
import { Observable, of } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  private productCollection: AngularFirestoreCollection<Product>;

  private duration = environment.timeoutDuration;

  constructor(private fireStore: AngularFirestore) {
    this.productCollection = this.fireStore.collection<Product>('products');
  }

  getProducts(): Observable<Product[] | string> {
    return this.productCollection.valueChanges().pipe(
      timeout(this.duration),
      catchError(error => of(`Request timed out after ${this.duration} ms`))
    );
  }

  getProductsByCategory(categoryId: string): Observable<Product[] | string> {
    const collection: AngularFirestoreCollection<
      Product
    > = this.fireStore.collection('products', ref =>
      ref.where('category', '==', categoryId)
    );
    return collection.valueChanges().pipe(
      timeout(this.duration),
      catchError(error => of(`Request timed out after ${this.duration} ms`))
    );
  }

  getProduct(productId: string) {
    return this.productCollection.doc(productId).valueChanges();
  }

  updateProduct(product: Product) {
    let id = product.id;

    if (!id || id.trim() === '') {
      id = this.fireStore.createId();
      product.id = id;
    }

    return this.productCollection.doc(id).set(product);
  }

  deleteProduct(productId: string) {
    return this.productCollection.doc(productId).delete();
  }
}
