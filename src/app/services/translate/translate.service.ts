import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  AngularFirestoreCollection,
  AngularFirestore,
} from '@angular/fire/firestore';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TranslateService {
  private localisationCollection: AngularFirestoreCollection<any>;

  private chosenLanguage: string;
  public translations: any;

  constructor(private fireStore: AngularFirestore) {
    this.localisationCollection = this.fireStore.collection<any>(
      'localisation'
    );
  }

  use(lang?: string): Promise<any> {
    this.chosenLanguage = lang || 'pt';

    return this.getLocalisationStrings();
  }

  async getLocalisationStrings() {
    const coll = await this.localisationCollection
      .get()
      .pipe(take(1))
      .toPromise();

    const langs = coll.docs.map(doc => doc.id);

    const docsContent = await this.localisationCollection
      .valueChanges()
      .pipe(take(1))
      .toPromise();

    this.translations = {};

    langs.forEach((lang, i) => {
      this.translations[lang] = docsContent[i];
    });

    return this.translations;
  }

  translate(value: string): string {
    return this.translations[this.chosenLanguage][value] || value;
  }

  updateTranslations(lang: string, doc: any) {
    return this.localisationCollection.doc(lang).set(doc);
  }

  async deleteTranslation(lang: string, delKey: string) {
    const data = await this.getLocalisationStrings();

    const translations = data[lang];

    const newDoc = {};

    // tslint:disable-next-line:forin
    for (const key in translations) {
      if (key !== delKey) {
        newDoc[key] = translations[key];
      }
    }

    await this.updateTranslations(lang, newDoc);
  }
}
