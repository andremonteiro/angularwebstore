import { Injectable } from '@angular/core';
import {
  AngularFirestoreCollection,
  AngularFirestore,
} from '@angular/fire/firestore';
import { Category } from 'src/app/view-model';
import { TranslateService } from '../translate/translate.service';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { timeout, catchError, take } from 'rxjs/operators';
import { ProductsService } from '../products/products.service';

@Injectable({
  providedIn: 'root',
})
export class CategoriesService {
  private categoryCollection: AngularFirestoreCollection<Category>;

  private duration = environment.timeoutDuration;

  constructor(
    private fireStore: AngularFirestore,
    private productsService: ProductsService,
    private translate: TranslateService
  ) {
    this.categoryCollection = this.fireStore.collection<Category>('categories');
  }

  getCategories(): Observable<Category[] | string> {
    return this.categoryCollection.valueChanges().pipe(
      timeout(this.duration),
      catchError(error => of(`Request timed out after ${this.duration} ms`))
    );
  }

  getCategory(categoryId: string) {
    return this.categoryCollection.doc(categoryId).valueChanges();
  }

  updateCategory(category: Category): Promise<void> {
    let id = category.id;

    if (!id || id.trim() === '') {
      id = this.fireStore.createId();
      category.id = id;
    }

    return this.categoryCollection.doc(id).set(category);
  }

  async deleteCategory(categoryId: string) {
    const categories: any = await this.getCategories()
      .pipe(take(1))
      .toPromise();
    const children = categories.find(c => c.parent == categoryId);
    if (children) {
      return {
        warning: true,
        data: this.translate.translate('TOAST_CATEGORY_DELETE_CHILDREN_EXIST'),
      };
    }

    const products = await this.productsService
      .getProductsByCategory(categoryId)
      .pipe(take(1))
      .toPromise();
    if (products && products.length > 0) {
      return {
        warning: true,
        data: this.translate.translate('TOAST_CATEGORY_DELETE_PRODUCTS_EXIST'),
      };
    }

    await this.categoryCollection.doc(categoryId).delete();

    return await this.getCategories()
      .pipe(take(1))
      .toPromise();
  }
}
