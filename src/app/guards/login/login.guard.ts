import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild,
  Router,
} from '@angular/router';
import { Observable, from } from 'rxjs';
import { AuthService, LoadingService } from 'src/app/services';
import { take, map, tap } from 'rxjs/operators';
import { User } from 'src/app/view-model';
import { SettingsService } from 'src/app/services';

@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanActivate, CanActivateChild {
  constructor(
    private auth: AuthService,
    private settings: SettingsService,
    private router: Router,
    private loading: LoadingService
  ) {}

  async loginGuardCondition() {
    this.loading.show();

    const settings = await this.settings.getSettings();

    if (!settings.auth.isLoginAvailable) {
      console.log('Login is not available!');
      this.router.navigate(['/']);
      return false;
    }

    const isNotLoggedIn = await this.auth.user
      .pipe(
        take(1),
        map((user: User) => {
          if (user === null) {
            return true;
          }
          return false;
        })
      )
      .toPromise();

    if (!isNotLoggedIn) {
      console.log('User is already logged in');
      this.router.navigate(['/']);
      return false;
    }

    return true;
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.loginGuardCondition();
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    return this.canActivate(childRoute, state);
  }
}
