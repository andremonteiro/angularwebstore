import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, APP_INITIALIZER } from '@angular/core';

import { AppComponent } from './app.component';

import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { appRoutes } from './app.routes';
import {
  TranslateService,
  CategoriesService,
  ProductsService,
  AuthService,
  UsersService,
  SettingsService,
  LoadingService,
  ModalService,
} from './services';

import localePT from '@angular/common/locales/pt-PT';
import { registerLocaleData } from '@angular/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { SharedModule } from './modules';

import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';

registerLocaleData(localePT);

export function setupTranslateFactory(service: TranslateService): Function {
  return () => service.use('pt');
}

export function setupSettingsFactory(service: SettingsService): Function {
  return () => service.getSettings();
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(environment.toastr),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FormlyModule.forRoot(),
    FormlyBootstrapModule,
    SharedModule,
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'pt-PT',
    },
    Title,
    {
      provide: APP_INITIALIZER,
      useFactory: setupTranslateFactory,
      deps: [TranslateService],
      multi: true,
    },
    TranslateService,
    LoadingService,
    ModalService,
    ProductsService,
    CategoriesService,
    AuthService,
    UsersService,
    {
      provide: APP_INITIALIZER,
      useFactory: setupSettingsFactory,
      deps: [SettingsService],
      multi: true,
    },
    SettingsService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
