export interface CarouselItem {
  title: string;
  subtitle: string;
  imageUrl: string;
  url: string;
}
