export interface User {
  displayName: string;
  email: string;
  isAdmin: boolean;
  photoURL: string;
  loginDate: any;
  uid: string;
  wishlist?: string[];
}
