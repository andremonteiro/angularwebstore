export interface ModalData {
  title: string;
  description: string;
  buttonPrimary?: {
    text: string;
    class?: string;
  };
}
