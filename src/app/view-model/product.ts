export interface Product {
  id: string;
  category: string;
  coupon: string;
  description: string;
  imageUrl: string;
  name: string;
  oldPrice: number;
  price: number;
  storeName: string;
  url: string;
}
