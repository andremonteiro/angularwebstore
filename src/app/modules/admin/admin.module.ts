import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '..';
import { RouterModule } from '@angular/router';
import { adminRoutes } from './admin.routes';
import {
  AdminComponent,
  AdminDashboardComponent,
  AdminCategoriesComponent,
  AdminProductsComponent,
  AdminCategoriesEditComponent,
  AdminProductsEditComponent,
  AdminLocalisationComponent,
  AdminLocalisationEditComponent,
  AdminUsersComponent,
  AdminSettingsComponent,
} from 'src/app/components/admin';
import {
  AdminNavbarComponent,
  AdminSidebarComponent,
  NavbarComponent,
} from 'src/app/layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { FormlyModule } from '@ngx-formly/core';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(adminRoutes),
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,
    FormlyModule.forChild(),
    CKEditorModule,
  ],
  declarations: [
    AdminComponent,
    AdminDashboardComponent,
    AdminNavbarComponent,
    AdminSidebarComponent,
    AdminProductsComponent,
    AdminCategoriesComponent,
    AdminCategoriesEditComponent,
    AdminProductsEditComponent,
    AdminLocalisationComponent,
    AdminLocalisationEditComponent,
    AdminUsersComponent,
    AdminSettingsComponent,
  ],
  providers: [],
})
export class AdminModule {}
