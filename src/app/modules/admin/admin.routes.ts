import { Routes } from '@angular/router';
import {
  AdminComponent,
  AdminDashboardComponent,
  AdminCategoriesComponent,
  AdminProductsComponent,
  AdminCategoriesEditComponent,
  AdminProductsEditComponent,
  AdminLocalisationComponent,
  AdminLocalisationEditComponent,
  AdminUsersComponent,
  AdminSettingsComponent,
} from 'src/app/components/admin';
import {
  CategoriesResolver,
  ProductsResolver,
  TranslationsResolver,
  SettingsResolver,
} from 'src/app/resolvers';
import { AuthGuard } from 'src/app/guards';
import { UsersResolver } from 'src/app/resolvers/users/users.resolver';

export const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'categories/:id',
        component: AdminCategoriesEditComponent,
        resolve: {
          categories: CategoriesResolver,
        },
      },
      {
        path: 'categories',
        component: AdminCategoriesComponent,
        resolve: {
          categories: CategoriesResolver,
          // settings: SettingsResolver,
        },
      },
      {
        path: 'products/:id',
        component: AdminProductsEditComponent,
        resolve: {
          categories: CategoriesResolver,
          products: ProductsResolver,
        },
      },
      {
        path: 'products',
        component: AdminProductsComponent,
        resolve: {
          products: ProductsResolver,
          // settings: SettingsResolver,
        },
      },
      {
        path: 'localisation/:lang/:id',
        component: AdminLocalisationEditComponent,
        resolve: {
          translations: TranslationsResolver,
        },
      },
      {
        path: 'localisation',
        component: AdminLocalisationComponent,
        resolve: {
          translations: TranslationsResolver,
        },
      },
      {
        path: 'users',
        component: AdminUsersComponent,
        resolve: {
          users: UsersResolver,
        },
      },
      {
        path: 'settings',
        component: AdminSettingsComponent,
        resolve: {
          settings: SettingsResolver,
        },
      },
      {
        path: 'dashboard',
        component: AdminDashboardComponent,
      },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: '**', redirectTo: 'dashboard', pathMatch: 'full' },
    ],
  },
];
