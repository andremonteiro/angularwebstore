import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  StoreComponent,
  FrontPageComponent,
  DetailPageComponent,
  LoginComponent,
  ProfileComponent,
} from 'src/app/components/store';
import { RouterModule } from '@angular/router';
import { storeRoutes } from './store.routes';
import {
  NavbarComponent,
  CarouselComponent,
  SidebarComponent,
  ProductGridComponent,
  FooterComponent,
} from 'src/app/layout';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '..';
import { FormlyModule } from '@ngx-formly/core';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(storeRoutes),
    SharedModule,
    ReactiveFormsModule,
    FormlyModule.forChild(),
  ],
  declarations: [
    StoreComponent,
    FrontPageComponent,
    NavbarComponent,
    CarouselComponent,
    SidebarComponent,
    ProductGridComponent,
    FooterComponent,
    DetailPageComponent,
    LoginComponent,
    ProfileComponent,
  ],
})
export class StoreModule {}
