import { Routes } from '@angular/router';
import {
  StoreComponent,
  FrontPageComponent,
  DetailPageComponent,
  LoginComponent,
} from 'src/app/components/store';
import {
  ProductsResolver,
  CategoriesResolver,
  UsersResolver,
  ProfileResolver,
} from 'src/app/resolvers';
import { LoginGuard } from 'src/app/guards';
import { ProfileComponent } from 'src/app/components/store/profile/profile.component';

export const storeRoutes: Routes = [
  {
    path: '',
    component: StoreComponent,
    children: [
      {
        path: 'product/:id',
        component: DetailPageComponent,
        resolve: {
          products: ProductsResolver,
          categories: CategoriesResolver,
        },
      },
      {
        path: 'profile',
        component: ProfileComponent,
        resolve: {
          products: ProductsResolver,
          user: ProfileResolver,
        },
      },
      {
        path: 'login',
        component: LoginComponent,
        canActivate: [LoginGuard],
      },
      {
        path: '',
        component: FrontPageComponent,
        resolve: {
          products: ProductsResolver,
          categories: CategoriesResolver,
        },
      },
    ],
  },
];
