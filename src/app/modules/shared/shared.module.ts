import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslatePipe } from 'src/app/pipes';
import {
  LoadingComponent,
  ModalComponent,
  SplashScreenComponent,
} from 'src/app/layout';

@NgModule({
  imports: [CommonModule],
  declarations: [
    TranslatePipe,
    LoadingComponent,
    ModalComponent,
    SplashScreenComponent,
  ],
  exports: [
    TranslatePipe,
    LoadingComponent,
    ModalComponent,
    SplashScreenComponent,
  ],
})
export class SharedModule {}
