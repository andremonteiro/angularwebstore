import { Component, OnInit } from '@angular/core';
import { TranslateService, LoadingService, SettingsService } from './services';
import { Title } from '@angular/platform-browser';
import { Router, NavigationEnd, NavigationCancel } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  showSplash = true;
  enabledSplash = false;

  constructor(
    private router: Router,
    private title: Title,
    private translate: TranslateService,
    private settings: SettingsService
  ) {}

  ngOnInit() {
    this.title.setTitle(this.translate.translate('APP_NAME'));

    this.enabledSplash = this.settings.getSetting('store', 'showSplash');

    const s = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd || event instanceof NavigationCancel) {
        this.showSplash = false;
        s.unsubscribe();
      }
    });
  }
}
